# Text to Speech and Speech to Text Demo with IFLYTEK
This is a Text to Speech and Speech to Text Demo with IFLYTEK SDK, wraped with Python, running on Raspberry Pi.

Try it out!

```
python3 src/test_msp.py
```

Hotword detection with [Snowboy](https://snowboy.kitt.ai)

```
python3 src/demo.py path_to_your_model_file
```

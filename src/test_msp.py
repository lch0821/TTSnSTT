#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
from MSP import msp


if __name__ == "__main__":
    if len(sys.argv) == 2:
        text = sys.argv[1]
    else:
        text = """测试"""

    wave_file = open("wav/iflytek01.wav", 'rb')
    wave_file.seek(0, os.SEEK_END)
    len = wave_file.tell()
    wave_file.seek(0, os.SEEK_SET)
    pcm = wave_file.read(len)

    if msp.login() == 0:
        res = msp.isr(pcm)
        print('STT: %s' % res)
        print(text)
        msp.tts(text.encode('U8'))
        msp.logout()
    else:
        print('Login failed')
